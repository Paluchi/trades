#pragma once
#include <string>
#include <vector>
#include <shared_mutex>
#include <future>
#include "rapidjson/document.h"

enum class Side : char {
    BID,
    ASK,
    UNKNOWN
};
struct Trade
{
    double mPrice;
    double mAmount;
    Side mSide;
    Trade(double price, double amount, Side side)
        :mPrice(price),
        mAmount(amount),
        mSide(side)
    {}
};

class Trades final {

public:
    Trades(const char* endpoint);
    void startTrading();
    const double mRate{0.001};

private:
    const char* mEndpoint;
    Trades() = default;
    void populateSide(const rapidjson::Value& side, std::vector<Trade>& trades, std::shared_mutex& mtx, std::promise<void>& prmse, Side sType);
    std::string getTrades();
    void parseTrades();
    void calculateTrades();
    void displayTrade(const Trade& trade) const;
    auto averageTrade(const std::vector<Trade>& trades, std::shared_mutex& mtx) -> std::unique_ptr<Trade>;
    auto weightedAverageSide(const std::vector<Trade>& trades, std::shared_mutex& mtx) -> std::unique_ptr<Trade>;

    std::promise<void> mBidsP;
    std::promise<void> mAsksP;

    std::future<void> mBidsFTask;
    std::future<void> mAsksFTask;

    std::vector<Trade> mBids;
    std::vector<Trade> mAsks;

    std::shared_mutex mBidsMtx;
    std::shared_mutex mAsksMtx;



};
