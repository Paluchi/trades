#include "Trades.h"
#include <iostream>
#include <curl/curl.h>
#include <string>
#include <iomanip> 

rapidjson::Document doc;

static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

static std::string getSideString(Side side)
{
    switch (side)
    {
        case Side::BID: 
            return "Bid";
        case Side::ASK:
            return "Ask";
        default:
            return "Unknown";
    }
}

Trades::Trades(const char* endpoint)
    :mEndpoint(endpoint)
{}

void Trades::startTrading()
{
    parseTrades();
    calculateTrades();
}

void Trades::populateSide(const rapidjson::Value& sides, std::vector<Trade>& trades, std::shared_mutex& mtx, std::promise<void>& prmse, Side sType)
{
    std::unique_lock<std::shared_mutex> lck(mtx);
    for (rapidjson::SizeType i = 0; i < sides.Size(); i++)
    {
        const rapidjson::Value& side = sides[i];
        if (side.HasMember("price") && side.HasMember("amount"))
        {
            if (side["price"].IsDouble() && side["amount"].IsDouble())
            {
                trades.emplace_back(side["price"].GetDouble(),
                    side["amount"].GetDouble(),
                    sType);
            }
            else if (side["price"].IsString() && side["amount"].IsString())
            {
                try {
                    const double& sidePrice = std::stod(side["price"].GetString());
                    const double& sideAmount = std::stod(side["amount"].GetString());
                    trades.emplace_back(sidePrice, sideAmount, sType);
                }
                catch (std::exception& e)
                {
                    prmse.set_exception(std::current_exception());
                }
            }
        }
    }
}

std::string Trades::getTrades()
{
    CURL* curl;
    CURLcode res;
    std::string readBuffer{};

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, mEndpoint);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }
    return readBuffer;
}

void Trades::parseTrades()
{
    const std::string trades = std::move(getTrades());
    rapidjson::Document doc;
    doc.Parse(trades.c_str());
    if (doc.IsObject())
    {
        if (doc.HasMember("bids"))
        {
            if (doc["bids"].IsArray())
            {
                rapidjson::Value& bids = doc["bids"];
                mBidsFTask = std::async(std::launch::async, &Trades::populateSide, this, std::ref(bids), std::ref(mBids),std::ref(mBidsMtx), std::ref(mBidsP), Side::BID);
                
            }
        }
        if (doc.HasMember("asks"))
        {
            if (doc["asks"].IsArray())
            {
                rapidjson::Value& asks = doc["asks"];
                mAsksFTask = std::async(std::launch::async, &Trades::populateSide,this, std::ref(asks), std::ref(mAsks), std::ref(mAsksMtx), std::ref(mAsksP), Side::ASK);
            }
        }
        
        if (mBidsFTask.valid())
        {
            mBidsFTask.wait();
        }
        if (mAsksFTask.valid())
        {
            mAsksFTask.wait();
        }/**/
    }
}

void Trades::calculateTrades()
{  
    auto wBidF = std::async(std::launch::async, &Trades::weightedAverageSide, this, std::ref(mBids), std::ref(mBidsMtx));
    auto wAskF = std::async(std::launch::async, &Trades::weightedAverageSide, this, std::ref(mAsks), std::ref(mAsksMtx));

    auto avBidF = std::async(std::launch::async, &Trades::averageTrade, this, std::ref(mBids), std::ref(mBidsMtx));
    auto avAskF = std::async(std::launch::async, &Trades::averageTrade, this, std::ref(mAsks), std::ref(mAsksMtx));

    std::cout << "\n*****Weighted Trades*****\n";
    displayTrade(*(wBidF.get()));
    displayTrade(*(wAskF.get()));
    std::cout << "\n*****Average Trades*****\n";
    displayTrade(*(avBidF.get()));
    displayTrade(*(avAskF.get()));
}

auto Trades::averageTrade(const std::vector<Trade>& trades, std::shared_mutex& mtx) -> std::unique_ptr<Trade>
{
    std::shared_lock<std::shared_mutex> shrdLck(mtx);
    double price{}, amt{};
    Side side{ Side::UNKNOWN };

    for (const auto& trade : trades)
    {
        price += trade.mPrice;
        amt += trade.mAmount;
    }
    if (!trades.empty())
    {
        price /= trades.size();
        amt /= trades.size();
        side = trades.front().mSide;
    }

    return std::make_unique<Trade>(price, amt, side);
}

auto Trades::weightedAverageSide(const std::vector<Trade>& trades, std::shared_mutex& mtx) -> std::unique_ptr<Trade>
{
    std::shared_lock<std::shared_mutex> lck(mtx);
    double wPrice{}, wAmt{};   
    double totalAmt{ 0.0 }, wghtSumPrice{ 0.0 };
    double totalPrice{ 0.0 }, wghtSumAmt{ 0.0 };
    Side side{ Side::UNKNOWN };

    for (const auto& trade : trades)
    {
        totalAmt += trade.mAmount;
        wghtSumPrice += trade.mPrice * trade.mAmount;
        
        totalPrice += trade.mPrice;
        wghtSumAmt += trade.mAmount * trade.mPrice;
    }

    if (0 < totalAmt && 0 < totalPrice)
    {
        wPrice = wghtSumAmt / totalAmt;
        wAmt = wghtSumPrice / totalPrice;
        side = trades.front().mSide;
    }

    return std::make_unique<Trade>(wPrice, wAmt, side);
   // return Trade && ();
}

void Trades::displayTrade(const Trade& trade) const
{
    double price{ trade.mPrice };
    double amt{ trade.mAmount };
    Side side{ trade.mSide };

    
    if (0 < price && 0 < amt && Side::UNKNOWN != side)
    {
        std::cout << "\n" << getSideString(side) << std::setprecision(11) << "\tPrice:\t" << price << "\tAmount:\t" << amt << "\n";
        double ratedPrice{ 0.0 }, ratedAmt{ 0.0 };
        ratedAmt = amt -= (amt * mRate);
        if (Side::ASK == trade.mSide)
        {
            ratedPrice = price+=(price * mRate);
        }
        else
        {
            ratedPrice = price -= (price * mRate);
        }
        std::cout << "\nRATED Trades at:\t" <<mRate <<"\t" << getSideString(side) <<std::setprecision(11)<< "\tPrice:\t" << price << "\tAmount:\t" << amt << "\n";

    }
}