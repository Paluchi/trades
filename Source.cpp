#include "Trades.h"
#include <iostream>

int main(int argc, char* argv[])
{
    const char* endpoint{ "https://api.bitfinex.com/v1/book/ethbtc" }; 
    Trades trade(endpoint);
    trade.startTrading();
    return 0;
}